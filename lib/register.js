const _configs = require('./configs');

let configs = _configs.get();

let selectors = {
	inputs: {
		username: 'input[name=os_username]',
		password: 'input[name=os_password]',
		taskDate: '[name=txtData]',
		taskContrato: '[name=cboCliContrProj]',
		taskEtapa: '[name=cboEtapa]',
		taskFase: '[name=cboFase]',
		taskKey: '[name=txtCdTarefaJira]',
		taskActivity: '[name=txtAtividade]',
		taskHour: '[name=txtHoraInicio]',
		taskQtdHora: '[name=txtQtHora]',
	},
	btns: {
		login: '#login',
		consultTask: 'a[href="javascript:psqTarefa();"]',
		taskSend: '[name=btIncluir]',
	},
	links: {
		preencher: 'a[href="ativ_inclusao.asp"]',
	},
};

let tasksToDo = [
	{
		date: '29/07/2017',
		contrato: 'tresc',
		type: 'setup',
		task: '',
		descricao: 'Teste de Task',
		inicio: '10:00',
		horas: '1'
	}
];

/* ----------------------------- */

let etapas = {
	setup: 'Construção',
	manutencao: 'Manutenção',
	bill: 'Manutenção',
	leader: 'Construção',
};

let success = 0;

/* ---------- functions ----------- */
function findByOptions(options, ...args) {
  let current = 0;
  let optionfinal = options;

  while (_.size(optionfinal) > 1) {
    console.log(args[current]);
    optionfinal = _.pickBy(optionfinal, (val) => {
      return val.indexOf(args[current]) > -1;
    });

    current++;
  }

  if (_.size(optionfinal) == 0) {
    return false;
  }

  return (_.size(optionfinal) == 1) ? optionfinal : false;
}
/* ---------- end functions ----------- */

var Nightmare = require('nightmare');
var $ = require('jquery');
var nightmare = Nightmare({ show: true });

if (!!tasksToDo.length) {
	console.log('Iniciando...');

	let afazer = tasksToDo.length;
	let contratos = [];

	nightmare
	.goto('https://lab.accurate.com.br/accweb/')
	.insert(selectors.inputs.username, configs.user)
	.insert(selectors.inputs.password, configs.pass)
	.click(selectors.btns.login)
	.wait(selectors.links.preencher)
	.click(selectors.links.preencher)
	.wait(selectors.inputs.taskDate)
	.inject('js', 'node_modules/jquery/dist/jquery.min.js');

	for (let task in tasksToDo) {

		nightmare
		.evaluate((selectors) => {
			// console.log(selectors);
			let options = [];

			let $select = $(selectors.inputs.taskContrato);
			$select.find('option').each((i, e) => {
				options[$(e).attr('value')] = $(e).text();
			});

			return findByOptions(task.contrato, task.type);
		}, selectors)
		.then(function (text) {
			console.log(text);
		});

	}

	// nightmare
	// .end()
	// .then(function () {
	// 	console.log('Fim! Tasks ' + success + ' de ' + afazer + ' com sucesso.');
	// })
	// .catch(function (error) {
	// 	console.error('Task failed:', error);
	// });


	// let afazer = tasksToDo.length;
	// let success = 0;
	// for (let i = 0; i < afazer; i++) {
	// 	nightmare
	// 	.type(selectors.inputs.taskDate, tasksToDo.date)
	// 	.evaluate(() => {

	// 		return
	// 	})
	// 	.then(() => {

	// 	})
	// }
} else {
	console.log('No tasks to do!');
}